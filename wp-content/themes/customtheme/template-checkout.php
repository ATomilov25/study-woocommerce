<?php
/*
    Template Name: Checkout
*/
get_header( 'shop' );
the_post();
the_content();
get_footer( 'shop' );
?>
